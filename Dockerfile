FROM sysrun/rpi-rtl-sdr-base:latest

MAINTAINER Frederik Granna

WORKDIR /tmp

RUN git clone https://github.com/comcat/acarsx.git && \
    cd acarsx && \
    make

WORKDIR /tmp/acarsx

ENTRYPOINT ["./acarsx"]
